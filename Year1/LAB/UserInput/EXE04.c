#include <stdio.h>

int main(){
    int score;
    printf("Input Your score: ");
    scanf("%d",&score);
    if(score >= 0  && score <= 49){
        printf("Your Grade is F");
    }else if(score >= 50  && score <= 59){
        printf("Your Grade is D");
    }else if(score >= 60  && score <= 69){
        printf("Your Grade is C");
    }else if(score >= 70 && score <= 79){
        printf("Your Grade is B");
    }else if(score >= 80  && score <= 100){
        printf("Your Grade is A");
    }else{
        printf("The Score should between 0 to 100");
    }
}