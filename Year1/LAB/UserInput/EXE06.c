#include <stdio.h>
#include <math.h>

int main(){
    int a,b,c;
    int d;
    float data;
    float x1,x2;
    printf("Calculate Quadratic Equation ax^2+bx+c=0\n");
    printf("Input a: ");
    scanf("%d",&a);
    printf("Input b: ");
    scanf("%d",&b);
    printf("Input c: ");
    scanf("%d",&c);
    d=b*b - (4*a*c);
    data=sqrt(d);
    x1=(-b-data)/(2*a);
    x2=(-b+data)/(2*a);
    printf("x1= %.2f \nx2= %.2f",x1,x2);

}