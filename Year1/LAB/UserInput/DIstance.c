#include <stdio.h>
#include <math.h>

int main(){
    int x1,y1,z1;
    int x2,y2,z2;
    int d;
    float d2;
    float a;
    printf("Enter the first point: "),scanf("%d %d %d",&x1,&y1,&z1);
    printf("Enter the second point: "),scanf("%d %d %d",&x2,&y2,&z2);
    a=1.0;
    d=((x1-x2)*(x1-x2))+((y1-y2)*(y1-y2))+((z1-z2)*(z1-z2));
    d2=sqrt(d)*a;
    printf("The distance between (%d, %d, %d) and (%d, %d, %d) is %.2f",x1,y1,z1,x2,y2,z2,d2);
}