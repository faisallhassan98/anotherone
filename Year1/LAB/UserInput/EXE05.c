#include <stdio.h>
#include <math.h>

int main(){
    int x1,y1,x2,y2;
    float d;
    int xsquare;
    int ysquare;
    int total;
    printf("Input x1: ");
    scanf("%d",&x1);
    printf("Input y1: ");
    scanf("%d",&y1);
    printf("Input x2: ");
    scanf("%d",&x2);
    printf("Input y2: ");
    scanf("%d",&y2);
    xsquare=pow(x2-x1,2);
    ysquare=pow(y2-y1,2);
    total=xsquare+ysquare;
    d=sqrt(total);
    printf("Distance is %f",d);
}