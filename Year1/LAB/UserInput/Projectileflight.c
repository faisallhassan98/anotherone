#include <stdio.h>
#include <math.h>

int main(){
    int g;
    float beta;
    int v; //v=velocity
    float h; //h=heigh
    int d; //d=distance
    float t; //t=time
    printf("Input an angle in radian: "),scanf("%f",&beta);
    printf("Input a distance to target: "),scanf("%d",&d);
    printf("Input a projectile velocity: "),scanf("%d",&v);
    g=32.17;
    t=d/(v*cos(beta));
    h=(v*sin(beta)*t)-((g*t*t)/2);
    printf("The flight will take %.2f second.\n",t);
    printf("The height at impact is %.2f feet.",h);  
}