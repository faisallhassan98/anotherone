#include <stdio.h>

struct Class{
    char className[10];
    int door;
    int window; 
};

int main(){
    struct Class class;

    struct Class maximum;
    int i;

    for(i=1; i<6; i++){
        printf("Enter class %d: ",i);
        scanf("%s %d %d", &class.className, &class.door, &class.window);
        
        if (i == 1 || (maximum.door + maximum.window ) < (class.window + class.door)){
            maximum = class;
        }
    }
    printf("The clas with highest equipment is %s, %d, %d", maximum.className, maximum.door, maximum.window);

}