#include <stdio.h>
#include <string.h>

struct Car {
    char name[10];
    int year;
    int speed;
};

int main(){
    struct Car Car;
    
    struct Car Carhighestspeed;
    int highestspeed = 0;
    int i;

    for(i=1; i<=5; i++){
        printf("Enter Car %d: ", i);
        scanf("%s %d %d", &Car.name,&Car.year,&Car.speed);
        if (highestspeed < Car.speed){
            highestspeed = Car.speed;
            strcpy(Carhighestspeed.name,Car.name);
            Carhighestspeed.speed = Car.speed;
        }
    }
    printf("The fastest speed is %s, %d", Carhighestspeed.name, Carhighestspeed.speed);

}