#include <stdio.h>

float Input(){
    float a;
    printf("Enter a temperature in F: "),scanf("%f",&a);
    return a;
}

float convert(float a){
    float v;
    v=((a-32)*5)/9;
    return v;
}

int main(){
  float x;
  float V;
  x=Input();
  V=convert(x);
  printf("Temperature in C =  %.1f",V);
  return 0;
}