#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char *Input(){
    char *x=malloc(9);
    printf("Enter a number ('x' to exit): ");
    scanf("%s", x);
    return x;
}

void checknum(char in[]){
    int num = atoi(in);
    if(num == 0){
        return;
    }
    if(num %2 ==1){
       printf("%d is an odd number.\n\n",num);
    }else{
        printf("%d is an even number.\n\n",num);
    }
}

int main(){
    char in[9];
    while(1>0){
        strcpy(in, Input());
        if(strcmp(in, "x") == 0){
            printf("Bye!!!!");
            break;
        }checknum(in);
    }
  return 0;
}
