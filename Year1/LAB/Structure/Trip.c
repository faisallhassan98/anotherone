#include <stdio.h>

 struct cost{
        int d;
        int v;
        int c;
        int t;
       
    };

 int main(){

    struct cost trip1;
    printf("Input trip distance (km): ");
    scanf("%d",&trip1.d);
    printf("Input your drive speed (km/hr): ");
    scanf("%d",&trip1.v);
    trip1.t=trip1.d/trip1.v;
    if(trip1.v<90){
            trip1.c=(trip1.d/15)*30;
        }else if(trip1.v>=90 && trip1.v<150){
            trip1.c=(trip1.d/10)*30;
        }else if(trip1.v>150){
            trip1.c=(trip1.d/5)*30;
        }
    printf("Drive time is about %d hours and fuel cost is approximately %d $\n",trip1.t,trip1.c);
    printf("-------------------------------------------------------------------------");

}