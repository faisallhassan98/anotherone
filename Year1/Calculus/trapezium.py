from typing import Generator

try:
    import openpyxl
except ImportError:
    print("Please run command pip install openpyxl")

def load_xlsx_data(filepath : str) -> Generator:
    wb_obj = openpyxl.load_workbook(filepath)
    sheet = wb_obj.active

    for row in sheet.iter_rows(values_only=True):
        column_1 = row[0]
        column_2 = row[1]
        
        if column_1 == "x" and column_2 == "y":
            continue
        
        x = column_1
        y = column_2

        yield (float(x), float(y))
        
def main():
    filepath = "AUC_data.xlsx"
    data = list(load_xlsx_data(filepath = filepath))
    print(len(data))
    
    AUC = 0
    
    for index, i in enumerate(data):
        x = i[0]
        y = i[1]
        
        try:
            auc = (y + data[index + 1][1]) * (data[index + 1][0] - x) / 2
        except IndexError:
            pass
        
        AUC += auc
        
    print(f"The area under curve using trapezoidal rule is: {AUC}")
    
if __name__ == '__main__':
    main()
        
    